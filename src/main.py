#!/usr/bin/env python3
# CS1440 Assignment 1: Analyzing Large Quantities of Data Linearly
# Overview: Write a program that calculates the gross wage, establishments, and employment for all FIPS areas (except
# metropolitan areas and state-wide areas, to avoid double-counting values), and which area has the max for the value,
# and what the max value is. Calculate values for all industries (represented by own code 0 and industry code 10), and
# the software pubishing industry (represented by own code 5 and industry code 5112). Data is taken from the 2017 CSV
# file put together by the US Federal Government Bureau of Labor and Statistics and a CSV file connecting FIPS codes
# with their area names. All values should correlate with the Report file copied from Instructor Erik Falor's
# repository. The program is intended to run from the command line, so takes an argument for the data directory
# and hard-codes the title of the files. @katyaPilkington
import sys
from Report import Report
rpt = Report()

# Turn the area_titles.csv file into a searchable list by opening the file and iterating over each line. FIPS codes
# that contain non-numerical characters or end with '000' are not considered for the report, so it is unnecessary to
# write them to the list. @katyaPilkington


def fips_names(directory):
    dictionary1 = {}

    with open(directory + '/area_titles.csv') as fipsfile:
        for line in fipsfile:
            line = line.rstrip()
            line = line.replace('"', '')
            lines = line.split(',', 1)

            if line[0].isalpha() or line[0].endswith('000'):
                continue
            else:
                dictionary1[lines[0]] = lines[1]

    return dictionary1

# Iterate over the employment and wage data CSV file. Disregard lines where the FIPS code is not applicable, then note
# whether the own_code and industry_code match all industries or software publishing. Write lines that match proper
# code values to the appropriate list.


def industry_picking(directory):
    all_list = []
    soft_list = []
    with open(directory + '/2017.annual.singlefile.csv') as dataFile:
        for line in dataFile:
            line = line.rstrip()
            line = line.replace('"', '')
            line = line.split(',')
            if not line[0].isdigit() or line[0].endswith('000'):
                continue
            if (line[1] == '0') and (line[2] == '10'):
                new_line = [line[0], line[8], line[9], line[10]]
                all_list.append(new_line)
            elif (line[1] == '5') and (line[2] == '5112'):
                new_line = [line[0], line[8], line[9], line[10]]
                soft_list.append(new_line)
            else:
                continue

    return all_list, soft_list


# def wage_calculation(list1, list2,):
#     gross_wage = 0
#     for line in list1:
#         # wage = int(line[10])
#         wage = int(line[3])
#         gross_wage += wage
#
#         list2.append([line[1], line[0]])
#         list2.sort(reverse=True)
#         # Make a list of lists: newline = [line[3], line[0]]
#         # if wage > list2[1]:
#         #     list2[0] = line[0]
#         #     list2[1] = wage
#
#     return gross_wage
#
#
# def establishment_calculation(list1, list2):
#     gross_establishments = 0
#
#     for line in list1:
#         # establishment = int(line[8])
#         establishment = int(line[1])
#         gross_establishments += establishment
#
#         list2.append([line[1], line[0]])
#         list2.sort(reverse=True)
#         # Make a list of lists: newline = [line[1], line[0]]
#         # if establishment > list2[1]:
#         #     list2[0] = line[0]
#         #     list2[1] = establishment
#
#     return gross_establishments
#
#
# def employment_calculation(full_list, employment_list):
#     gross_employment = 0
#     for line in list1:
#         # employment = int(line[9])
#         employment = int(line[2])
#         gross_employment += employment
#
#         list2.append([line[2], line[0]])
#         list2.sort(reverse=True)
#         # Make a list of lists sorted by value: newline = [line[2], line[0]]
#         # if employment > list2[1]:
#         #     list2[0] = line[0]
#         #     list2[1] = employment
#     return gross_employment


def type_calculator(industry_list, type_list, type):
    if type == "pay": index = 3
    elif type == "estab": index = 1
    else: index = 2
    gross = 0
    for line in industry_list:
        type_value = int(line[index])
        gross += type_value

        type_list.append([int(line[index]), line[0]])
        type_list.sort(reverse=True)
    return gross

def name_that_area(dictionary1, list2):
    area_name = dictionary1[list2[1]]
    return area_name


def distinct_values(value_list):
    distinct_set = set()
    for line in value_list:
        distinct_set.add(line[0])
    return len(distinct_set)


def unique_values(value_list):
    unique_dict = {}
    for line in value_list:
        if line[0] in unique_dict:
            unique_dict[line[0]] += 1
        else:
            unique_dict[line[0]] = 1

    total_unique = 0
    for value in unique_dict.values():
        if value == 1:
            total_unique += 1

    return total_unique


def per_capita_average(gross_employment, gross_pay):
    capita_average = gross_pay / gross_employment
    return capita_average


def type_median(type_list):
    length = len(type_list)
    index1 = (length // 2) - 1
    index2 = length // 2
    if (length % 2) == 0:
        median = type_list[index2][0] + type_list[index1][0] / 2
    else:
        median =  type_list[index1][0]
    return median


def top_5_areas(list1, dict1):
    # These are lists of 5 pairs (2-tuples): 0th element is a FIPS area name, 1th element is a numeric value
    top5 = []
    for i in range(0, 5):
        top5.append([name_that_area(dict1, list1[i]), list1[i][0]])
    return top5


def cache_rank(type_list):
    cache_fips = '49005'
    rank = 0
    for line in type_list:
        if line[1] == cache_fips:
            break
        rank += 1

    return rank

def assign_vales(industry, type):
    industry_type = []
    rpt.industry.total_type = type_calculator(industry, industry_type, type)
    rpt.industry.distinct_type = distinct_values(industry_type)
    rpt.industry.unique_type = unique_values(industry_type)
    rpt.industry.median_type = type_median(industry_type)
    # rpt.industry.cache_co_type_rank = cache_rank(industry_type)
    # rpt.industry

def main():

    datadir = sys.argv[1]
    fips_areas = fips_names(datadir)

    all_industries, software_publishing = industry_picking(datadir)

    rpt.all.count = len(all_industries)
    rpt.soft.count = len(software_publishing)

    # assign_vales('all', 'pay')


    all_pay = []
    rpt.all.total_pay = type_calculator(all_industries, all_pay, "pay")
    rpt.all.distinct_pay = distinct_values(all_pay)
    rpt.all.unique_pay = unique_values(all_pay)
    rpt.all.median_pay = type_median(all_pay)
    rpt.all.cache_co_pay_rank = cache_rank(all_pay)

    all_estab = []
    rpt.all.total_estab = type_calculator(all_industries, all_estab, "estab")
    rpt.all.distinct_estab = distinct_values(all_estab)
    rpt.all.unique_estab = unique_values(all_estab)
    rpt.all.median_estab = type_median(all_estab)
    rpt.all.cache_co_estab_rank = cache_rank(all_estab)

    all_empl = []
    rpt.all.total_empl = type_calculator(all_industries, all_empl, "empl")
    rpt.all.distinct_empl = distinct_values(all_empl)
    rpt.all.unique_empl = unique_values(all_empl)
    rpt.all.median_empl = type_median(all_empl)
    rpt.all.cache_co_empl_rank = cache_rank(all_empl)

    rpt.all.per_capita_avg_wage = per_capita_average(rpt.all.total_empl, rpt.all.total_pay)

    rpt.all.top_annual_wages = top_5_areas(all_pay, fips_areas)
    rpt.all.top_annual_avg_emplvl = top_5_areas(all_empl, fips_areas)
    rpt.all.top_annual_estab = top_5_areas(all_estab, fips_areas)

    # all_max_wage = [0, 0]
    # rpt.all.total_pay = wage_calculation(all_industries, all_max_wage)
    # # rpt.all.max_pay = name_that_area(fips_areas, all_max_wage)
    #
    # all_max_establishments = [0, 0]
    # rpt.all.total_estab = establishment_calculation(all_industries, all_max_establishments)
    # # rpt.all.max_estab = name_that_area(fips_areas, all_max_establishments)
    #
    # all_max_employment = [0, 0]
    # rpt.all.total_empl = employment_calculation(all_industries, all_max_employment)
    # # rpt.all.max_empl = name_that_area(fips_areas, all_max_employment)
    #
    # software_max_wage = [0, 0]
    # rpt.soft.total_pay = wage_calculation(software_publishing, software_max_wage)
    # # rpt.soft.max_pay = name_that_area(fips_areas, software_max_wage)
    #
    # software_max_establishments = [0, 0]
    # rpt.soft.total_estab = establishment_calculation(software_publishing, software_max_establishments)
    # # rpt.soft.max_estab = name_that_area(fips_areas, software_max_establishments)
    #
    # software_max_employment = [0, 0]
    # rpt.soft.total_empl = employment_calculation(software_publishing, software_max_employment)
    # # rpt.soft.max_empl = name_that_area(fips_areas, software_max_employment)
    soft_pay = []
    rpt.soft.total_pay = type_calculator(software_publishing, soft_pay, "pay")
    rpt.soft.distinct_pay = distinct_values(soft_pay)
    rpt.soft.unique_pay = unique_values(soft_pay)
    rpt.soft.median_pay = type_median(soft_pay)
    rpt.soft.cache_co_pay_rank = cache_rank(soft_pay)

    soft_estab = []
    rpt.soft.total_estab = type_calculator(software_publishing, soft_estab, "estab")
    rpt.soft.distinct_estab = distinct_values(soft_estab)
    rpt.soft.unique_estab = unique_values(soft_estab)
    rpt.soft.median_estab = type_median(soft_estab)
    rpt.soft.cache_co_estab_rank = cache_rank(soft_estab)

    soft_empl = []
    rpt.soft.total_empl = type_calculator(software_publishing, soft_empl, "empl")
    rpt.soft.distinct_empl = distinct_values(soft_empl)
    rpt.soft.unique_empl = unique_values(soft_empl)
    rpt.soft.median_empl = type_median(soft_empl)
    rpt.soft.cache_co_empl_rank = cache_rank(soft_empl)

    rpt.soft.per_capita_avg_wage = per_capita_average(rpt.soft.total_empl, rpt.soft.total_pay)

    rpt.soft.top_annual_wages = top_5_areas(soft_pay, fips_areas)
    rpt.soft.top_annual_avg_emplvl = top_5_areas(soft_empl, fips_areas)
    rpt.soft.top_annual_estab = top_5_areas(soft_estab, fips_areas)

    print(rpt)


main()
